class User < ApplicationRecord
	before_save { self.email.downcase! }
	validates :name, presence: true
	em_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, length: { maximum: 255 },
	                                  format: { with: em_regex },
	                                  uniqueness: { case_sensitive: false }
	has_secure_password
	validates :password, presence: true, length: { minimum: 6 }, allow_nil: true


	def self.digest(token)
		BCrypt::Password.create(token)
	end

	def authentic?(token)
		BCrypt::Password.new(self.password_digest) == token
	end

	def certificate
		"?f=" + BCrypt::Password.create("9fj90k.'f9f9$imjlnfdiso9/%#,f&#{((Time.now.to_i)-95536).to_s}#{self.email}") + "&time=#{Time.now.to_i}&user=#{self.email}"
	end

	
end
