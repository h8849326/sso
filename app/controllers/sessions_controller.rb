class SessionsController < ApplicationController
  skip_before_filter :verify_authenticity_token, only: [:new, :create]
  def new
  	session[:url] = params[:url] 
    if params[:login] == "1"  # 系统发送请求验证是否已经登录
      if logged_in?
       @user = User.find(session[:user_id])
        redirect_to "#{url_root(session[:url])}login#{@user.certificate}&unlogin=1" 
      else
        redirect_to "#{session[:url]}?unlogin=2"
      end
    end

  end

  def create
	    @user = User.find_by(email: params[:login][:email])
      if @user.authentic? params[:login][:password] 
        log_in @user 
        redirect_to "#{url_root(session[:url])}login#{@user.certificate}&unlogin=3"  	
      else
        flash[:danger] = "登录失败，请检查用户名和密码"
        render 'new'

      end
  end

  def destroy
      session[:user_id] = nil
      @user = User.find_by(email params[:email])
      @user.online = false
      @user.save
  	
  end
end
