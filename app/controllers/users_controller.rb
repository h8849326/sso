class UsersController < ApplicationController
	def new
		@user = User.new
	end

	def show
	end

	def create
		@user = User.new(user_params)
		if @user.save
			
			flash[:success] = "新建用户成功"
		else
			flash[:danger] = "检查你的信息"
			redirect_to '/signup'
			
		end

	end


	private
		def user_params
			params.require(:user).permit(:name, :email, :password, :password_confirmation)
		end
end
