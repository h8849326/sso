class CreateSessionIds < ActiveRecord::Migration[5.0]
  def change
    create_table :session_ids do |t|
      t.string :s_id

      t.timestamps
    end
  end
end
