class AddOnlineToSessionIds < ActiveRecord::Migration[5.0]
  def change
    add_column :session_ids, :online, :boolean
  end
end
