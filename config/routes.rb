Rails.application.routes.draw do
  get 'sessions/new'

  get '/signup', 	to: 'users#new'
  post '/signup', 	to: 'users#create'	
  get '/login', 	to: 'sessions#new'
  post '/login', 	to: 'sessions#create'
  post '/logout', 	to: 'sessions#destroy'
  post'/loginre',   to: 'sessions#login_redirect'
  root 'application#index'
  resource :users
end
